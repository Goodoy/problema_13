/*Ejercicio que debe hallar estrellas en una imagen digitalizada de la cual resulta una matriz, se hace recorriendola
  realizando la formula entregada de que los valores de su alrededor mas ella / 5 de mas de 6, se recorre atreves de
  punteros que toman valores y usan el algrebra para rastreo, y luego verifica si es o no estrella, en caso de serla
  imprime cual es su centro y al final dice cuantas estrellas encontró*/

#include <iostream>

using namespace std;

void imprimir(int photo[][8]);
int cantidadEstrellas(int (*p1)[8]);
bool esEstrella(int centro, int at,int ad, int arr,int ab);



int main()
{
    int fotografia[][8]= {0, 3, 4, 0, 0, 0, 6, 8, //
                      5,13, 6, 0, 0, 0, 2, 3,     //
                      2, 6, 2, 7, 3, 0,10, 0,     //--> imagen
                      0, 0, 4,15, 4, 1, 6, 0,     //
                      0, 0, 7,12, 6, 9,10, 4,     //
                      5, 0, 6,10, 6, 4, 8, 0};    //
    int c=0;
    int (*pF)[8]=fotografia; //Puntero que referencia a fotografia
    imprimir(fotografia); //impresion del arreglo bidimencional
    c=cantidadEstrellas(pF);  //Uso de la cantidad de estrellas

    if(c>0){
        cout << "\nLa cantidad de estrellas es: "<<c<<endl; //Impresion de la salida
    }

    return 0;
}

void imprimir(int photo[][8]){

    cout <<"\t------ Imagen -------"<<endl;  //Encabezado
    for(int fila=0;fila<6;fila++){   //Recorre sus filas
        for(int pos = 0;pos<8;pos++){  //Recorre sus columnas
            if(photo[fila][pos]<10){  //Ajustes visuales
                cout<<"   ";
            }else{
                cout<<"  ";
            }
            cout<<photo[fila][pos]; //imprime el valor
        }
        cout << endl; //Salto de linea
    }
}

//*(*(p1+fila)+pos) --> fila:filas, pos:posicion
int cantidadEstrellas(int (*p1)[8]){
    int cant=0;
    for(int fila=1;fila<5;fila++){
        for(int pos = 1;pos<7;pos++){
            if(esEstrella(*(*(p1+fila)+pos),       //
                          *(*(p1+fila)+(pos-1)),   //
                          *(*(p1+fila)+(pos+1)),   //---->Posiciones de al rededor
                          *(*(p1+(fila-1))+pos),   //
                          *(*(p1+(fila+1))+pos))){ //

                cout<<"\nEstrella con centro en ["<<fila<<","<<pos<<"]"<<endl; //Si la encuentra imprime este formato

                cant++; //Numero de filas
            }
        }
    }
    return cant; //Numero de filas

}

bool esEstrella(int centro, int at,int ad, int arr,int ab){ //Formula para saber si es o no estrella
    if((centro+at+ad+arr+ab)/5.0>6){ //Si la sumatoria de las posiciones dividido 5.0 es mayor a 6.
        return true; //
    }else{
        return false;
    }
}
